package plensys.java.project.emptyproject.core;

import plensys.java.framework.core.FrameworkConsts;

/**
 * 
 * @author mohycan
 *
 */
public final class Consts extends FrameworkConsts {

	public static final String LOCALE_HU = "hu";
	public static final String LOCALE_EN = "en";
	
	//#####################################################################
	//############### 			GUI FUNCTION				###############
	//#####################################################################
	public static final String GUI_FUNCTION_CT_NYELV = "CtNyelvTabledLayout";
	
	public static final String GUI_FUNCTION_USER = "UserTabledLayout";
	
	//#####################################################################
	//############### 		GUI FUNCTION CAPTION			###############
	//#####################################################################
	//CODETABLE CAPTIOM
	public static final String GUI_FUNCTION_CAPTION_CT_NYELV = "GuiFunction.ctNyelv.caption";
	//#####################################################################
	//############### 			BEAN HELPER					###############
	//#####################################################################
	public static final String BEAN_HELPER_USER = "UserBeanHelper";
	
	public static final String BEAN_HELPER_CT_NYELV = "CtNyelvHelper";
	//#####################################################################
	//############### 		CONTROLLER HELPER				###############
	//#####################################################################
	
	//#####################################################################
	//############### 				LAYOUT					###############
	//#####################################################################
	
	//#####################################################################
	//############### 			CT TABLE					###############
	//#####################################################################
	public static final String TABLE_CT_NYELV = "CT_NYELV";
	
	private Consts() {
	}

}
