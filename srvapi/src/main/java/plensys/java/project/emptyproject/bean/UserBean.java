package plensys.java.project.emptyproject.bean;

import plensys.java.spark.bean.core.AbstractBean;


/**
 * Default User a loginhoz
 * @author deakz
 *
 */
public class UserBean extends AbstractBean {

	public static final String EMAIL = "email";
	
	private String email;
	
	private String name;
	
	private String address;
	
	private String phoneNumber;
	
	public UserBean(String email, String name, String address, String phoneNumber) {
		super();
		this.email = email;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public UserBean() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean equalsContent(Object obj) {
		if (!super.equalsContent(obj)) {
			return false;
		}
		UserBean other = (UserBean) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserBean [email=");
		builder.append(email);
		builder.append(", ");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
	
	
}
