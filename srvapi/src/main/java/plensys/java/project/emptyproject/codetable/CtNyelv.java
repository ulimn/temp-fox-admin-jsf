package plensys.java.project.emptyproject.codetable;

import plensys.java.components.codetable.annotation.CodeTableBean;
import plensys.java.components.codetable.bean.AbstractNamedCodeTableEntity;
import plensys.java.project.emptyproject.core.Consts;

/**
 * 
 * @author deakz
 *
 */
@CodeTableBean(caption = Consts.GUI_FUNCTION_CAPTION_CT_NYELV, tableName = Consts.TABLE_CT_NYELV)
public class CtNyelv extends AbstractNamedCodeTableEntity {

	public CtNyelv() {
		super();
	}
	
	public CtNyelv(AbstractNamedCodeTableEntity item) {
		super(item);
	}

	public CtNyelv(Long id, String kod, String nev) {
		super(id, kod, nev);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CtNyelv [");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
