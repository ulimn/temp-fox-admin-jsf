package plensys.java.project.emptyproject.listfilter;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.spark.core.listfilter.AbstractListFilter;

/**
 * 
 * @author deakz
 *
 */
public class UserListFilter extends AbstractListFilter<UserBean> {

	private String email;
	
	public UserListFilter() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
