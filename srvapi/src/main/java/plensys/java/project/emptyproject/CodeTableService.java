package plensys.java.project.emptyproject;

import java.util.List;

import plensys.java.components.codetable.CodeTableCoreService;
import plensys.java.components.codetable.bean.filter.DefaultNamedCodeTableListFilter;
import plensys.java.project.emptyproject.codetable.CtNyelv;

/**
 * 
 * @author deakz
 *
 */
public interface CodeTableService extends CodeTableCoreService {

	public List<CtNyelv> getCtNyelvList(DefaultNamedCodeTableListFilter<CtNyelv> filter);
	
}
