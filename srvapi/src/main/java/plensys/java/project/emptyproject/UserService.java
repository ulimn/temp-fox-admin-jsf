package plensys.java.project.emptyproject;

import java.util.List;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.project.emptyproject.listfilter.UserListFilter;

/**
 * 
 * @author deakz
 *
 */
public interface UserService {

	/**
	 * USER
	 */
	public UserBean getUser(Long id);
	public UserBean saveUser(UserBean user);
	public UserBean deleteUser(UserBean user);
	public List<UserBean> getUserList(UserListFilter listFilter);

}
