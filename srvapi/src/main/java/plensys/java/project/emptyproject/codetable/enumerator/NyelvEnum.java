package plensys.java.project.emptyproject.codetable.enumerator;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import plensys.java.project.emptyproject.codetable.CtNyelv;

public enum NyelvEnum {

	HU (1L),
	EN (2L);
	
	private static final Map<Long, NyelvEnum> ENUMMAP = new HashMap<Long, NyelvEnum>();
	private static final Map<Long, CtNyelv> CTMAP = new HashMap<Long, CtNyelv>();
	
	static {
		for (NyelvEnum t : EnumSet.allOf(NyelvEnum.class)) {
			ENUMMAP.put(t.getCtId(), t);
			CTMAP.put(t.getCtId(), new CtNyelv(t.getCtId(), "<ENUMCODE>", "<ENUMVALUE>"));			
		}
	}
	
	private Long ctId;

	private NyelvEnum(Long ctId) {
		this.ctId = ctId;
	}

	public Long getCtId() {
		return ctId;
	}

	public static NyelvEnum getByCtId(Long ctId) {
		return ENUMMAP.get(ctId);
	}

	public CtNyelv getCtNyelv() {
		return new CtNyelv(CTMAP.get(ctId));
	}
}
