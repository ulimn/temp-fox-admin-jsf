package plensys.java.project.emptyproject.core;

import plensys.java.framework.core.FrameworkCommon;

/**
 * Átalánosan használt metódusok gyűjteménye
 * 
 * @author Mohycan
 *
 */
public final class Common extends FrameworkCommon {

	private Common() {
	}

}
