package plensys.java.project.emptyproject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import plensys.java.components.codetable.bean.filter.DefaultNamedCodeTableListFilter;
import plensys.java.project.emptyproject.codetable.CtNyelv;
import plensys.java.spark.exception.ServiceException;

/**
 * 
 * @author deakz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DefaultContext.class})
@TransactionConfiguration(defaultRollback = true)
@Transactional(propagation = Propagation.REQUIRED)
public class CodeTableServiceImplIntegrationTest {

	@Autowired
	private CodeTableService codeTableService;
	
	@Test
	public void test_CtNyelv_CodeTableService() throws ServiceException {
		//LIST
		Assert.assertNotEquals("getCtNyelvList failed",  0, codeTableService.getCtNyelvList(new DefaultNamedCodeTableListFilter<CtNyelv>()).size());
	}
}
