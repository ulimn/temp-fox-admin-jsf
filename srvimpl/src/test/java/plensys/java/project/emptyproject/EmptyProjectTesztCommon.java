package plensys.java.project.emptyproject;

import plensys.java.project.emptyproject.bean.UserBean;

/**
 * 
 * @author deakz
 *
 */
public class EmptyProjectTesztCommon {

	private EmptyProjectTesztCommon() {
	}
	
	public static UserBean getUser(String email) {
		UserBean item = new UserBean();
		
		item.setEmail(email);
		
		return item;
	}
}
