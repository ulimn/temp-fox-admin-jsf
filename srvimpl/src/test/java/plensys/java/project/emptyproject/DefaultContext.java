package plensys.java.project.emptyproject;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import plensys.java.components.codetable.CodeTableCoreService;
import plensys.java.components.codetable.CodeTableCoreServiceImpl;
import plensys.java.components.codetable.dao.CodeTableCoreMapper;
import plensys.java.components.communication.CommunicationService;
import plensys.java.components.communication.CommunicationServiceImpl;
import plensys.java.components.localedata.core.LocaleDataService;
import plensys.java.components.security.AuthenticationService;
import plensys.java.components.security.AuthenticationServiceImpl;
import plensys.java.components.security.PasswordValidator;
import plensys.java.components.security.PasswordValidatorImpl;
import plensys.java.components.security.dao.AuthenticationMapper;
import plensys.java.framework.service.junittest.AbstractJUnitTestContext;
import plensys.java.project.emptyproject.dao.CodeTableMapper;
import plensys.java.project.emptyproject.dao.CommonMapper;
import plensys.java.project.emptyproject.dao.UserMapper;
import plensys.java.project.emptyproject.serviceimpl.DummyLocaleDataServiceImpl;
import plensys.java.spark.ServiceHelper;
import plensys.java.spark.ServiceHelperImpl;
import plensys.java.spark.dao.SparkCommonMapper;
import plensys.java.spark.dao.config.SparkSqlSessionFactory;

@Configuration
public class DefaultContext extends AbstractJUnitTestContext {

	@Override
	public Class<?>[] getMapperInterfaces() {
		return new Class[] { 
				AuthenticationMapper.class,
				SparkCommonMapper.class,
				//FRAMEWORK
				CodeTableCoreMapper.class,
				//EMPTY PROJECT
				CodeTableMapper.class,
				CommonMapper.class,
				UserMapper.class
			};
	}

    @Bean
    public SqlSessionFactory getSqlSessionFactory(DataSource dataSource) throws Exception {
		return SparkSqlSessionFactory.getSqlSessionFactory(dataSource);
    }

	@Bean
	public ServiceHelper getServiceHelper() {
		return new ServiceHelperImpl();
	}
	
	@Bean
	public CodeTableCoreService getCodeTableCoreService() {
		return new CodeTableCoreServiceImpl();
	}

	@Bean
	public CodeTableService getCodeTableService() {
		return new CodeTableServiceImpl();
	}
	
	@Bean
	public CommunicationService getCommunicationService() {
		return new CommunicationServiceImpl();
	}

	@Bean
	public AuthenticationService getAuthenticationService() {
		return new AuthenticationServiceImpl();
	}
	
	@Bean
	public PasswordValidator getPasswordValidator() {
		return new PasswordValidatorImpl();
	}
	
	@Bean
	public LocaleDataService getLocaleDataService() {
		return new DummyLocaleDataServiceImpl();
	}
	
	@Bean
	public UserService getUserService() {
		return new UserServiceImpl();
	}
}
