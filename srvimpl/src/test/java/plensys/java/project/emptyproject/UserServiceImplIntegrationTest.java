package plensys.java.project.emptyproject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.project.emptyproject.listfilter.UserListFilter;
import plensys.java.spark.exception.ServiceException;

/**
 * 
 * @author deakz
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DefaultContext.class})
@TransactionConfiguration(defaultRollback = true)
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImplIntegrationTest {

	@Autowired
	private UserService userService;
	
	@Test
	public void test_User_UserService() throws ServiceException {
		Assert.assertTrue(true);
		int deltaItemCount = userService.getUserList(new UserListFilter()).size();

		//INSERT
		UserBean user = userService.saveUser(EmptyProjectTesztCommon.getUser("e@mail.hu"));
		Assert.assertTrue("saveUser (insert) failed", user.getId() != null);

		//LIST
		Assert.assertEquals("getUserList failed", 1 + deltaItemCount, userService.getUserList(new UserListFilter()).size());
		
		//UPDATE
		user.setEmail("email@hu.com");
		user = userService.saveUser(user);
		UserBean user2 = userService.getUser(user.getId());
		Assert.assertTrue("saveUser (update) failed", user.equalsContent(user2));

		//DELETE
		user = userService.deleteUser(user);
		Assert.assertEquals("deleteUser failed", deltaItemCount, userService.getUserList(new UserListFilter()).size());
	}
}
