package plensys.java.project.emptyproject.serviceimpl;

import java.util.Locale;

import plensys.java.components.localedata.core.LocaleDataService;

/**
 * 
 * @author deakz
 *
 */
public class DummyLocaleDataServiceImpl implements LocaleDataService {

	@Override
	public String getCurrentUserId() {
		return null;
	}

	@Override
	public String getCurrentUserName() {
		return "UserName";
	}

	@Override
	public String getCurrentIpAddress() {
		return "localhost:8181";
	}

	@Override
	public String getCurrentSessionId() {
		return "sessionId";
	}

	@Override
	public Locale getCurrentLocale() {
		return Locale.UK;
	}

	@Override
	public String getCurrentUIId() {
		// TODO Auto-generated method stub
		return null;
	}

}
