package plensys.java.project.emptyproject.filtermap;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.spark.core.listfilter.ListFilterMap;
import plensys.java.spark.core.listfilter.ListFilterMap.ListFilterMapItem;

/**
 * 
 * @author deakz
 *
 */
public class UserServiceFilterMap {

	public static final ListFilterMap LIST_FILTER_MAP_USER = new ListFilterMap (
		new ListFilterMapItem(UserBean.EMAIL, "", "")
	);
	
}
