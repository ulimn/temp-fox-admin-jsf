package plensys.java.project.emptyproject.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.project.emptyproject.listfilter.UserListFilter;

/**
 * 
 * @author deakz
 *
 */
public interface UserMapper {

	/**
	 * USER
	 */
	public void insertUser(@Param("bean") UserBean user);
	public void updateUser(@Param("bean") UserBean user);
	public void deleteUser(@Param("bean") UserBean user);
	public UserBean getUser(Long id);
	public List<UserBean> getUserList(@Param("filter") UserListFilter filter);
}
