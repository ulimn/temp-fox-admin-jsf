package plensys.java.project.emptyproject.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import plensys.java.components.codetable.bean.filter.DefaultNamedCodeTableListFilter;
import plensys.java.project.emptyproject.codetable.CtNyelv;

/**
 * 
 * @author deakz
 *
 */
public interface CodeTableMapper {

	public List<CtNyelv> getCtNyelvValueSet(@Param("filter") DefaultNamedCodeTableListFilter<CtNyelv> filter);

}
