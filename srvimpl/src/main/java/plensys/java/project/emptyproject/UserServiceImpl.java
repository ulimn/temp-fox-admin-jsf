package plensys.java.project.emptyproject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import plensys.java.project.emptyproject.bean.UserBean;
import plensys.java.project.emptyproject.dao.UserMapper;
import plensys.java.project.emptyproject.filtermap.UserServiceFilterMap;
import plensys.java.project.emptyproject.listfilter.UserListFilter;
import plensys.java.spark.ServiceHelper;

/**
 * 
 * @author deakz
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private ServiceHelper serviceHelper;
	
	@Override
	public UserBean saveUser(UserBean user) {
		return serviceHelper.saveBean(user, bean -> userMapper.insertUser(bean), bean -> userMapper.updateUser(bean));
	}
	
	@Override
	public UserBean deleteUser(UserBean user) {
		return serviceHelper.deleteBean(user, bean -> userMapper.deleteUser(bean));
	}
	
	@Override
	public UserBean getUser(Long id) {
		return userMapper.getUser(id);
	}

	@Override
	public List<UserBean> getUserList(UserListFilter listFilter) {
		listFilter.getFilterBuilder().setFilterMap(UserServiceFilterMap.LIST_FILTER_MAP_USER);
		return userMapper.getUserList(listFilter);
	}

}
