package plensys.java.project.emptyproject.filtermap;

import plensys.java.components.codetable.filtermap.CodeTableCoreServiceFilterMap;
import plensys.java.spark.core.listfilter.ListFilterMap;

/**
 * 
 * @author deakz
 *
 */
public class CodeTableServiceFilterMap extends CodeTableCoreServiceFilterMap {

	public static final ListFilterMap LIST_FILTER_MAP_CT_NYELV = CodeTableCoreServiceFilterMap.LIST_FILTER_MAP_NAMED_CODE_TABLE;
}
