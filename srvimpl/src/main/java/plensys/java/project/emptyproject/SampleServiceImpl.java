package plensys.java.project.emptyproject;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import plensys.java.project.emptyproject.bean.UserBean;

@Service("sampleService")
public class SampleServiceImpl implements SampleService{

	@Override
	public List<UserBean> getUserList() {
		ArrayList<UserBean> userBeans = new ArrayList<>();
		userBeans.add(new UserBean("xxxxxx@gg.hu", "Jotokán Jolán", "4023 Debrecen Fő utca 20.", "30/236-8179"));
		userBeans.add(new UserBean("aaaaaa@gg.hu", "Sprint Elek", "1012 Budapest X utca 20.", "70/436-8179"));
		userBeans.add(new UserBean("bbbbbb@gg.hu", "Kiss Géza", "1234 Budapest Mosoly utca 20.", "60/276-8179"));
		userBeans.add(new UserBean("cccccc@gg.hu", "Béluci", "4023 Egyek Fő utca 20.", "40/236-9179"));
		userBeans.add(new UserBean("dddddd@gg.hu", "Egyed Dániel", "4023 Fót Rózsa utca 20.", "20/776-8179"));
		userBeans.add(new UserBean("eeeeee@gg.hu", "Mel Egon", "1023 Budapest Körte utca 20.", "10/239-889"));
		userBeans.add(new UserBean("ffffff@gg.hu", "Term Elek", "1124 Debrecen Alma utca 20.", "70/656-3179"));
		userBeans.add(new UserBean("gggggg@gg.hu", "Incze János", "3456 Mohács Gyik utca 20.", "20/136-2179"));
		return userBeans;
	}

}
