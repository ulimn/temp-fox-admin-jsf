package plensys.java.project.emptyproject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import plensys.java.components.codetable.CodeTableCoreServiceImpl;
import plensys.java.components.codetable.bean.filter.DefaultNamedCodeTableListFilter;
import plensys.java.project.emptyproject.codetable.CtNyelv;
import plensys.java.project.emptyproject.dao.CodeTableMapper;
import plensys.java.project.emptyproject.filtermap.CodeTableServiceFilterMap;

/**
 * 
 * @author deakz
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CodeTableServiceImpl extends CodeTableCoreServiceImpl implements CodeTableService {

	@Autowired
	private CodeTableMapper codeTableMapper;

	@Override
	public List<CtNyelv> getCtNyelvList(DefaultNamedCodeTableListFilter<CtNyelv> filter) {
		filter.getFilterBuilder().setFilterMap(CodeTableServiceFilterMap.LIST_FILTER_MAP_NAMED_CODE_TABLE);
		return codeTableMapper.getCtNyelvValueSet(filter);
	}
}
