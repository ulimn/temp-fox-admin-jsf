package plensys.java.project.emptyproject.dao.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import plensys.java.framework.service.persistence.MyBatisMapperConfigurer;
import plensys.java.project.emptyproject.dao.CodeTableMapper;
import plensys.java.project.emptyproject.dao.CommonMapper;
import plensys.java.project.emptyproject.dao.UserMapper;

/**
 * 
 * @author leg
 * DAO configuration
 *
 */
@Configuration
public class EmptyProjectDaoConfig {
	public static final String SQL_SESSION_FACTORY_NAME = "plensys.java.project.emptyproject.dao.config";

	@Bean
	public static MyBatisMapperConfigurer getEmptyProjectMapperConfigurer(@Qualifier(SQL_SESSION_FACTORY_NAME)SqlSessionFactory sqlSessionFactory) {
		MyBatisMapperConfigurer configurer = new MyBatisMapperConfigurer();
		configurer.setSqlSessionFactory(sqlSessionFactory);
		configurer.setMapperInterfaces(new Class[] { 
				CodeTableMapper.class,
				CommonMapper.class,
				UserMapper.class
		});
		return configurer;
	}
}

