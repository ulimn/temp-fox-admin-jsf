package plensys.java.emptyproject.bean.scopes;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@RequestScoped
@ManagedBean(name = "sampleRequestScopeBean")
public class SampleRequestScopeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4106259057605196782L;

	public SampleRequestScopeBean() {
	}

	@PostConstruct
	public void init() {
		System.out.println("New Instance SampleRequestScopeBean!");
	}


	private String username;

	public void printUserName(){
		System.out.println("PRINT USERNAME: "+username);
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
