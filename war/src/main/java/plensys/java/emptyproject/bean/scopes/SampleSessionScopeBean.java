package plensys.java.emptyproject.bean.scopes;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean(name = "sampleSessionScopeBean")
public class SampleSessionScopeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4106259057605196782L;

	public SampleSessionScopeBean() {
	}

	@PostConstruct
	public void init(){
		System.out.println("New Instance SampleSessionScopeBean!");
	}

	
	
	
}
