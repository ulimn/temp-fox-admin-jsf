package plensys.java.emptyproject.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import hu.jsf.columns.ColumnModel;
import hu.jsf.columns.ReflectionColumnModelBuilder;
import plensys.java.project.emptyproject.SampleService;
import plensys.java.project.emptyproject.bean.UserBean;

@ManagedBean
@ViewScoped
public class UsersViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<ColumnModel> columns = new ArrayList<ColumnModel>(0);

	@ManagedProperty(value = "#{sampleService}")
	SampleService sampleService;

	public void setSampleService(SampleService sampleService) {
		this.sampleService = sampleService;
	}

	@PostConstruct
	public void init() {
		columns = new ReflectionColumnModelBuilder(UserBean.class).setExcludedProperties("id", "creationDate",
				"createdByUserId", "modificationDate", "modifiedByUserId", "removeDate", "removedByUserId","uuid","rowCnt","requiredPropertySet").build();
	}

	public List<UserBean> getUsers() {
		return sampleService.getUserList();
	}

	public List<ColumnModel> getColumns() {
		return columns;
	}
}
