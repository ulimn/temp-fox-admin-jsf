package plensys.java.emptyproject.bean.scopes;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ViewScoped
@ManagedBean(name = "sampleViewScopeBean")
public class SampleViewScopeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4106259057605196782L;

	public SampleViewScopeBean() {
	}

	@PostConstruct
	public void init(){
		System.out.println("New Instance SampleViewScopeBean!");
	}

	
	
}
