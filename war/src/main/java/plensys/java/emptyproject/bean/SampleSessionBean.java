package plensys.java.emptyproject.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import plensys.java.project.emptyproject.SampleService;
import plensys.java.project.emptyproject.bean.UserBean;

@SessionScoped
@ManagedBean(name = "sampleSessionBean")
public class SampleSessionBean  implements Serializable{

	private static final long serialVersionUID = 1L;

	private String sessionMs;
	
	private List<UserBean> filteredUsers;
	
	@ManagedProperty(value = "#{sampleService}")
	SampleService sampleService;
	
	public SampleService getSampleService() {
		return sampleService;
	}

	public void setSampleService(SampleService sampleService) {
		this.sampleService = sampleService;
	}

	public String getSessionMs() {
		return sessionMs;
	}

	public void setSessionMs(String sessionMs) {
		this.sessionMs = sessionMs;
	}

	public List<UserBean> getUserList(){
	 return	sampleService.getUserList();
	}
	
	@PostConstruct
	public void init(){
		sessionMs = "Hellllóóóó33333dddddd"   ;
	}
	public void changeString(){
		sessionMs = "Hellllóóóó222222222222222222222222233333333333333";
	}

	public List<UserBean> getFilteredUsers() {
		return filteredUsers;
	}

	public void setFilteredUsers(List<UserBean> filteredUsers) {
		this.filteredUsers = filteredUsers;
	}
	
}
