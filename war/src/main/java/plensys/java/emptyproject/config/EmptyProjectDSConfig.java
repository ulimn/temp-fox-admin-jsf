package plensys.java.emptyproject.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import plensys.java.components.codetable.dao.config.CodeTableDaoConfig;
import plensys.java.components.i18n.dao.config.I18nDaoConfig;
import plensys.java.components.lockhandler.dao.config.LockHadlerDaoConfig;
import plensys.java.components.security.dao.config.SecurityDaoConfig;
import plensys.java.components.systemparameter.dao.config.SystemParameterDaoConfig;
import plensys.java.framework.dao.config.FrameworkDaoConfig;
import plensys.java.project.emptyproject.dao.config.EmptyProjectDaoConfig;
import plensys.java.spark.dao.config.SparkDaoConfig;
import plensys.java.spark.dao.config.SparkSqlSessionFactory;

@Configuration
public class EmptyProjectDSConfig {
    private static final String DATASOURCE = "EmptyProjectDS";

    @Bean(name = {
    		EmptyProjectDaoConfig.SQL_SESSION_FACTORY_NAME,
    		FrameworkDaoConfig.SQL_SESSION_FACTORY_NAME,
    		I18nDaoConfig.SQL_SESSION_FACTORY_NAME,
    		LockHadlerDaoConfig.SQL_SESSION_FACTORY_NAME,
    		CodeTableDaoConfig.SQL_SESSION_FACTORY_NAME,
    		SecurityDaoConfig.SQL_SESSION_FACTORY_NAME,
    		SparkDaoConfig.SQL_SESSION_FACTORY_NAME,
    		SystemParameterDaoConfig.SQL_SESSION_FACTORY_NAME
    	}
    )
    public SqlSessionFactory getSqlSessionFactory(@Qualifier(DATASOURCE) DataSource dataSource) throws Exception {
		return SparkSqlSessionFactory.getSqlSessionFactory(dataSource);
    }
}
