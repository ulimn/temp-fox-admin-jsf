package plensys.java.emptyproject.service;

import org.springframework.stereotype.Service;

import plensys.java.spark.core.project.ProjectInformationService;

@Service
public class ProjectInformationServiceImpl implements ProjectInformationService{

	@Override
	public String getProjectId() {
		return "Jsf-Sandbox";
	}

}
