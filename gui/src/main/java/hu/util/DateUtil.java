package hu.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;

public class DateUtil {
	
	private static Map<Locale, String> datePatterns = new HashMap<Locale, String>();
	private static Locale DEFAULT_LOCALE = new Locale("hu", "HU");
	
	static{
		datePatterns.put(new Locale("hu", "HU"), "yyyy.MM.dd.");
		datePatterns.put(new Locale("en", "GB"), "dd/MM/yyyy");
		datePatterns.put(new Locale("cs", "CZ"), "yyyy.MM.dd");
		datePatterns.put(new Locale("sk", "SK"), "yyyy.MM.dd");
	}

        public static Date addDaysToDate(Date date, long days) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, new Long(days).intValue());
            return calendar.getTime();
        }
    
	public static Boolean isAdult(Date birthDate) {
		Calendar currentCalendar = dateToCalendarWithoutHours(getCurrentDate());
		currentCalendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR) - 18);
		Calendar birthCalendar = dateToCalendarWithoutHours(birthDate);
		return birthCalendar.getTimeInMillis()<=currentCalendar.getTimeInMillis();
	}
	
	public static String formatDate(java.util.Date date) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy");
		return formatter.format(date);
	}
	
	public static String formatDateByLocale(Date date){
		if(date == null){
			return "";
		}
		
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		String pattern = datePatterns.get(locale);
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern == null ? datePatterns.get(DEFAULT_LOCALE): pattern);
		
		return sdf.format(date);
	}
	
	public static String formatDateAndTime(Date date) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy / HH:mm");
		return formatter.format(date);
	}
	
	public static Date getCurrentDate()	{
		return new GregorianCalendar().getTime();
	}
	
	public static GregorianCalendar dateToCalendarWithoutHours(Date date) {
		if (date == null) {
			return null;
		}

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(java.util.Calendar.HOUR_OF_DAY, 0);
        calendar.set(java.util.Calendar.MINUTE, 0);
        calendar.set(java.util.Calendar.SECOND, 0);
        calendar.set(java.util.Calendar.MILLISECOND, 0);

        return calendar;
	}
	
	public static Date getCurrentDateWithoutHours() {
		java.util.Calendar today = java.util.Calendar.getInstance();
	    today.set(java.util.Calendar.HOUR_OF_DAY, 0);
	    today.set(java.util.Calendar.MINUTE, 0);
	    today.set(java.util.Calendar.SECOND, 0);
	    today.set(java.util.Calendar.MILLISECOND, 0);
	    return today.getTime();
	}
	
}
