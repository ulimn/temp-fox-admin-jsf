package hu.util;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@RequestScoped
@ManagedBean(name="clientSessionBean")
public class ClientSessionTimeout implements Serializable{
	
	private static final long serialVersionUID = 80116547175216410L;

	public Integer getSessionMaxInactiveInterval(){
		HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return httpSession.getMaxInactiveInterval();
	}
	
	public String getSiteDomain(){
		return "http://" + DomainUtil.getDomain();
	}

}
