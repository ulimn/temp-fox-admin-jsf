package hu.util;


public class CodeToNameUtil{
    
    @Deprecated
    public static String convert(String code){
        if( code != null ){
        
            if( code.equals("DIAMOND 12 MONTH WITH SMARTCARD") )  return "Gyémánt 12 hónapos, kártyával";
            if( code.equals("DIAMOND 6 MONTH WITH SMARTCARD") )  return "Gyémánt 6 hónapos, kártyával";
            if( code.equals("GOLD 12 MONTH WITH SMARTCARD") )  return "Arany 12 hónapos, kártyával";
            if( code.equals("GOLD 6 MONTH WITH SMARTCARD") ) return "Arany 6 hónapos, kártyával";
            if( code.equals("SILVER 12 MONTH WITH SMARTCARD") )  return "Ezüst 12 hónapos, kártyával";
            if( code.equals("SILVER 6 MONTH WITH SMARTCARD") )  return "Ezüst 6 hónapos, kártyával";
        }
        
        return code+".";
    }
}
