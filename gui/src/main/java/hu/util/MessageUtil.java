package hu.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class MessageUtil {

	private static ResourceBundle resources;

	static {
		resources = ResourceBundle.getBundle("Backoffice", Locale.getDefault());
	}

	public static void showMessage(Severity level, String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage(level, "", getResource(key));
		facesContext.addMessage(null, facesMessage);
	}

	public static String getResource(String key) {
		return resources.getString(key);
	}

	public static void setLocale(Locale locale) {
		resources = ResourceBundle.getBundle("Backoffice", locale);
	}

}
