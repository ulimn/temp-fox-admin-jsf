package hu.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.log.Log;

@FacesConverter("dateConverter")
public class DateConverter implements Converter {

	private String[] hunPatterns = { "yyyy.MM.dd", "yyyy.MM.dd." };

	private String[] enPatterns = { "dd/MM/yy", "dd/MM/yy/" };

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Locale locale = context.getViewRoot().getLocale();

		if (locale.getLanguage().equals("hu")) {
			return formatDate(hunPatterns, value);
		} else if (locale.getLanguage().equals("en")) {
			return formatDate(enPatterns, value);
		} else {
			try {
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
				df.setLenient(false);
				Date date = df.parse(value);
				return date;
			} catch (ParseException pe) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonMessageUtil.getMessage("global.invalid-date-format"), null);
				throw new ConverterException(msg);
			}
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Locale locale = context.getViewRoot().getLocale();
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);

		Date date = (Date) value;

		return df.format(date);
	}

	private Date formatDate(String[] patterns, String value) {

		Date date = checkPattern(patterns, value, true);
		if (date == null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonMessageUtil.getMessage("global.invalid-date-format"), null);
			throw new ConverterException(msg);
		}

		date = checkPattern(patterns, value, false);
		if (date == null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonMessageUtil.getMessage("global.non-existent-date"), null);
			throw new ConverterException(msg);
		}

		return date;
	}

	private Date checkPattern(String[] patterns, String value, boolean lenient) {
		for (String pattern : patterns) {
			DateFormat df = new SimpleDateFormat(pattern);
			df.setLenient(lenient);
			ParsePosition pos = new ParsePosition(0);
			Date result = df.parse(value, pos);
			//a dátumban szereplő fölösleges karakterek vizsgálására. Pl 2011.11.11.5646
			if (pos.getIndex() == value.length()) {
				return result;
			}
		}
		return null;
	}
}
