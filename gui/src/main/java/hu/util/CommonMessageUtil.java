package hu.util;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class CommonMessageUtil {
    public static final String DEFAULT_MESSAGE_BUNDLE = "msg";

	public static String getMessage(String var, String key){
		String message = "???"+var+":"+key+"???";
		try{
		message = FacesContext.getCurrentInstance().getApplication().
				getResourceBundle(FacesContext.getCurrentInstance(), var).getString(key);
		}catch(Exception e){

		}
		return message;
	}

	public static String getMessage(String key){
		return getMessage(DEFAULT_MESSAGE_BUNDLE, key);
	}

	public static void addFacesMessage(String clientid, Severity severity, String summary, String detail){
		FacesMessage fm = new FacesMessage();
		fm.setSeverity(severity);
		fm.setSummary(summary);
		fm.setDetail(detail);
		FacesContext.getCurrentInstance().addMessage(clientid, fm);
	}

	public static FacesMessage createMessage(Severity severity, String key){
		FacesMessage fm = new FacesMessage();
		fm.setSeverity(severity);
		fm.setSummary(getMessage(DEFAULT_MESSAGE_BUNDLE, key));
		fm.setDetail("");

		return fm;
	}

	public static void addFacesMessage(String clientid, Severity severity, String summary){
		addFacesMessage(clientid, severity, summary, "");
	}

	public static void addMessage(String clientid, Severity severity, String var, String key){
		addFacesMessage(clientid, severity, getMessage(var, key) );
	}

        public static void addMessage(Severity severity, String key) {
            addFacesMessage(null, severity, getMessage(DEFAULT_MESSAGE_BUNDLE, key));
        }
}
