package hu.util;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RequestScoped
@ManagedBean(name = "sessionHandler")
public class SessionHandler implements Serializable {

	private static final long serialVersionUID = -4976785107293714493L;

	/**
	 * Invalidáljuk a sessiont
	 */
	public void invalidateSession() {
		System.out.println("Invalidate session");

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession();
		session.invalidate();

	}

}
