package hu.util;

public class LanguageUtil {

	private LanguageUtil() {
	}

	public static String getDefiniteArticle(String word, String langCode) {
		if (langCode.equalsIgnoreCase("hu")) {
			if (word.matches("^[aáeéiíoóöőuúüűAÁEÉIÍÖŐUÚÜŰ].*")) {
				return "az";
			}
			return "a";
		} else if (langCode.equalsIgnoreCase("en")) {
			return "the";
		} else if (langCode.equalsIgnoreCase("cs") || langCode.equalsIgnoreCase("sk")) {
			return "";
		} else {
			throw new RuntimeException(
					"getDefiniteArticle is not yet implemented for language code: "
							+ langCode);
		}
	}

	/**
	 * Visszaadja a word tárgyas ragozását a langCode alapján.
	 */
	public static String getSubjectSuffix(String word, String langCode) {
		if (langCode.equalsIgnoreCase("hu")) {
			if (word.matches(".*[aáeéiíoóöőuúüűAÁEÉIÍÖŐUÚÜŰ]$")) {
				return "t";
			} else {
				for (int i = word.length() - 1; i >= 0; i--) {
					String c = new Character(word.charAt(i)).toString();
					if (c.matches("[aáoóöőuúüűAÁÖŐUÚÜŰ]")) {
						return "ot";
					}
					if (c.matches("[eéEÉ]")) {
						return "et";
					}
					if (c.matches("[iíIÍ]")) {
						return "it";
					}
				}
				return "t";
			}
		} else if (langCode.equalsIgnoreCase("en") || langCode.equalsIgnoreCase("cs") || langCode.equalsIgnoreCase("sk")) {
			return "";
		} else {
			throw new RuntimeException("getDefiniteArticle is not yet implemented for language code: " + langCode);
		}
	}
}
