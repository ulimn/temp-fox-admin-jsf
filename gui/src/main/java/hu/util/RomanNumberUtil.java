package hu.util;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class RomanNumberUtil {

	private static Map<String, Integer> romanToArabic;
	
	public static enum SortingOrder{
		ASCENDING(1), DESCENDING(-1);
		
		private int intValue;
		
		SortingOrder(int intValue){
			this.intValue = intValue;
		}
		
		public int getIntValue(){
			return intValue;
		}
	};
	
	static{
		init();
	}
	
	private static void init(){
		romanToArabic = new HashMap<String, Integer>();
		romanToArabic.put("I", 1);
		romanToArabic.put("IV", 4);
		romanToArabic.put("V", 5);
		romanToArabic.put("IX", 9);
		romanToArabic.put("X", 10);
		romanToArabic.put("XL", 40);
		romanToArabic.put("L", 50);
		romanToArabic.put("XC", 90);
		romanToArabic.put("C", 100);
		romanToArabic.put("CD", 400);
		romanToArabic.put("D", 500);
		romanToArabic.put("CM", 900);
		romanToArabic.put("M", 1000);
	}
	
	public static Integer getArabicValue(String roman){
		Integer arabic = 0;
		
		int index = 0;
		while(index < roman.length()){
			if((index + 1) < roman.length()){
				String num1 = String.valueOf(roman.charAt(index));
				String num2 = String.valueOf(roman.charAt(index + 1));
				
				if(romanToArabic.get(num1) < romanToArabic.get(num2)){
					arabic += romanToArabic.get(num1 + num2);
					index += 2;
				} else{
					arabic += romanToArabic.get(num1);
					index++;
				}
			} else {
				String num = String.valueOf(roman.charAt(index));
				arabic += romanToArabic.get(num);
				index++;
			}
		}
		
		return arabic;
	}
	
	public static void sort(List<String> romanNumbers){
		doSort(romanNumbers, SortingOrder.ASCENDING);
	}
	
	public static void sort(List<String> romanNumbers, SortingOrder sortingOrder){
		doSort(romanNumbers, sortingOrder);
	}
	
	private static void doSort(List<String> romanNumbers, final SortingOrder sortingOrder){
		if(romanToArabic == null){
			init();
		}
		
		Collections.sort(romanNumbers, new Comparator<String>() {

			@Override
			public int compare(String number1, String number2) {
				return getArabicValue(number1).compareTo(getArabicValue(number2));
			}
		});
		
	}
	
}
