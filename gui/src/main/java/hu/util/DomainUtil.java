package hu.util;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@RequestScoped
@ManagedBean(name = "domainUtil")
public class DomainUtil implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2510554706966710763L;

	public static String getDomain() {
		String siteDomain = null;

		// Megnézzük, hogy PageFilter rakott-e a session-be
//		siteDomain = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(Param247Filter.PARAM_NAME_SITE_DOMAIN);

		// Ha nem találtunk, akkor használjuk az aktuális hívás domainját
		// Ez elég durva!!!
		if (siteDomain == null || siteDomain.isEmpty()) {
			siteDomain = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
		}

		return siteDomain;

	}


	/**
	 * Visszaadja a főoldal címet
	 *
	 * @return
	 */
	public static String getMainPage() {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		final String mainPageUrl = ((httpServletRequest.isSecure()) ? "https" : "http") + "://" + getDomain();
		return mainPageUrl;
	}

	/**
	 * Domain függő szövegekhez domain.key -ként kell megadni a property kulcsát
	 * a fileban így keresi, ha nem találja, akkor a keyt keresi és azt adja
	 * vissza.
	 *
	 * @param bundleName
	 * @param key
	 * @return
	 */
	public String getPropertyByDomain(final String bundleName, final String key) {
		final String domain = getDomain();
		final FacesContext facesContext = FacesContext.getCurrentInstance();
		String stringByDomain = "";

		final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, bundleName);

		try {
			stringByDomain = bundle.getString(domain + "." + key);
		} catch (Exception e) {
			// nincs meg a domain.key alapján a szöveg
			stringByDomain = bundle.getString(key);
		}

		return stringByDomain;
	}

}