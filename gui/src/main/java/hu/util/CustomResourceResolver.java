package hu.util;

import java.net.URL;

import javax.faces.view.facelets.ResourceResolver;

public class CustomResourceResolver extends ResourceResolver {

	private ResourceResolver parent;
	private String basePath;

	public CustomResourceResolver(ResourceResolver parent) {
		this.parent = parent;
		this.basePath = "/META-INF/resources"; // TODO: Make configureable?
	}

	@Override
	public URL resolveUrl(String path) {
		URL url = getClass().getResource(basePath + path);

		if (url == null) {
			url = parent.resolveUrl(path);

		}

		return url;
	}

}