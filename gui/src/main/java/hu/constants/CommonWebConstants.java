package hu.constants;

public class CommonWebConstants {

	public static final String REQUEST_PARAM_ORDER_ID_HASH = "order";
	public static final String REQUEST_PARAM_ORDER_ID = "order-id";
	
	public static final String SESSION_PARAM_ORDER_ID = "orderId";
	
}
